package com.demo.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;

/**
 * Html to PDF
 *
 */
public class Html2PdfApp
{
    /**
     * Print Help message when try to start the application with invalid parameters
     * 
     * @param error
     */
    protected static void printHelp(String error) {
        if (error != null && !error.isEmpty()) {
            System.out.println("Error: " + error);
        }

        System.out.println("Usage:");
        System.out.println("com.demo.app.Html2PdfApp [src.html] [dest.pdf]");
    }

    public static void main( String[] args)
    {
        if (args.length < 2) {
            printHelp("Can`t initialize properly Html2Pdf due to invalid number of arguments: " + args.length);
            return;
        }

        final String src = args[0];
        final String dest = args[1];

        try {
            File file = new File(dest);
            file.getParentFile().mkdirs();
            PdfDocument pdfDoc = new PdfDocument(new PdfWriter(dest));
            pdfDoc.setDefaultPageSize(PageSize.A4);

            ConverterProperties properties = new ConverterProperties();

            FileInputStream is = new FileInputStream(src);
            HtmlConverter.convertToPdf(is, pdfDoc, properties);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
