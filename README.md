# it7Demo

Test of iText7 html to prf conversion

## Starting the project

To run the project use:
```sh
git clone git@gitlab.com:ctsvetkov/it7demo.git
cd ./it7demo
./mvn package
java -cp target/it7demo-jar-with-dependencies.jar com.demo.app.Html2PdfApp ./files/test.html ./files/result.pdf

```